package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/beego/beego/v2/client/orm"

	"github.com/beego/beego/v2/core/config"
	"github.com/goombaio/namegenerator"

	_ "github.com/mattn/go-sqlite3"

	sendgrid "github.com/sendgrid/sendgrid-go"
	mail "github.com/sendgrid/sendgrid-go/helpers/mail"
)

func init() {
	// Create DB
	os.MkdirAll("./data/dev/", 0755)
	os.Create("./data/dev/sendgrid.db")

	orm.RegisterDriver("sqlite3", orm.DRSqlite)
	orm.RegisterDataBase("default", "sqlite3", "./data/dev/default.db")
	orm.RegisterDataBase("sendgriddb", "sqlite3", "./data/dev/sendgrid.db")
}

func main() {
	// TODO config + RunSyncDb to func initDb()
	// Load configs
	iniconf, err := config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		fmt.Println(err)
	}

	db_boot, err := iniconf.String("db::beego_db_bootstrap")
	if err != nil {
		fmt.Println(err)
	}
	db_debug, err := iniconf.String("db::beego_db_debug")
	if err != nil {
		fmt.Println(err)
	}

	if db_debug == "true" {
		orm.Debug = true
	}

	// orm.RunSyncdb needs to run at every startup
	if db_boot == "true" {
		name := "sendgriddb"
		force := true
		verbose := true
		err := orm.RunSyncdb(name, force, verbose)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		name := "sendgriddb"
		force := false
		verbose := false
		err := orm.RunSyncdb(name, force, verbose)
		if err != nil {
			fmt.Println(err)
		}
	}

	// Enable cmd: orm syncdb
	orm.RunCommand()
	fillTable(30)

	// ---------------------------- Sendgrid-------------------------------------

	iniconf, err = config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		log.Println(err)
	}

	sg_api_key, err := iniconf.String("sendgrid::beego_sg_api_key")
	if err != nil {
		log.Println(err)
	}

	// Step 1: -------------------- Send Email-----------------------------------
	from := mail.NewEmail("Your Support Team", "support@treesnqs.org")
	subject := "Sending with SendGrid: html content"
	to := mail.NewEmail("Peter Pan", "olmighty99@gmail.com")
	// plainTextContent := "and easy to do anywhere, even with Go"
	htmlContent := "<strong>and easy to do anywhere, even with Go</strong>"
	message := mail.NewSingleEmail(from, subject, to, "", htmlContent)

	// -> /v3/mail/send
	sg_client := sendgrid.NewSendClient(sg_api_key)
	response, err := sg_client.Send(message)
	if err != nil {
		log.Printf("ERROR [*] Sending email failed.. %v", err)
	} else {
		log.Printf("INFO [*] Sendgrid response, status: %v", response.StatusCode)
		log.Printf("INFO [*] Sendgrid response, body: %v", response.Body)
		log.Printf("INFO [*] Sendgrid response, header: %#v", response.Headers)
	}

}

// Create any number of random users and store to db
func fillTable(num int) {
	answers := []string{
		"It is certain",
		"It is decidedly so",
		"Without a doubt",
		"Yes definitely",
		"You may rely on it",
		"As I see it yes",
		"Most likely",
		"Outlook good",
		"Yes",
		"Signs point to yes",
		"Reply hazy try again",
		"Ask again later",
		"Better not tell you now",
		"Cannot predict now",
		"Concentrate and ask again",
		"Don't count on it",
		"My reply is no",
		"My sources say no",
		"Outlook not so good",
		"Very doubtful",
	}
	emails := []string{
		"yahoo.com",
		"gmail.com",
		"yahoo.com",
		"hotmail.com",
		"aol.com",
		"hotmail.co.uk",
		"hotmail.fr",
		"msn.com",
		"yahoo.fr",
		"wanadoo.fr",
		"orange.fr",
		"comcast.net",
		"yahoo.co.uk",
		"yahoo.com.br",
		"yahoo.co.in",
		"live.com",
		"rediffmail.com",
		"free.fr",
		"gmx.de",
	}
	balances := randFloats(0.0000, 101.9899, 300)

	for i := 0; i < num; i++ {
		seed := time.Now().UTC().UnixNano()
		min := 10
		max := 80
		age := int16(rand.Intn(max-min) + min)
		create := randate()
		nameGenerator := namegenerator.NewNameGenerator(seed)
		name := nameGenerator.Generate()
		o := orm.NewOrmUsingDB("sendgriddb")

		profile := new(Profile)
		profile.Age = age
		profile.Answer = answers[rand.Intn(len(answers))]
		profile.Balance = balances[rand.Intn(len(balances))]
		profile.Created = create

		user := new(User)
		user.Profile = profile
		user.Name = name
		user.Email = name + "@" + emails[rand.Intn(len(emails))]

		_, _ = o.Insert(profile)
		_, _ = o.Insert(user)
	}
}

// return a random time.Time within a pre-defined period
func randate() time.Time {
	min := time.Date(2015, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2021, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

// return list of len n with random floats
func randFloats(min, max float64, n int) []float64 {
	res := make([]float64, n)
	for i := range res {
		res[i] = min + rand.Float64()*(max-min)
	}
	return res
}
