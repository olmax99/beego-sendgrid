package main

import (
	"time"

	"github.com/beego/beego/v2/client/orm"
)

type User struct {
	Id      int
	Name    string
	Email   string   `orm:"unique"`
	Profile *Profile `orm:"rel(one)"` // OneToOne relation
}

type Profile struct {
	Id      int
	Age     int16
	Answer  string
	Created time.Time `orm:"type(datetime);precision(2)"`
	Balance float64   `orm:"digits(12);decimals(4)"`
	User    *User     `orm:"reverse(one)"` // Reverse relationship (optional)
}

func init() {
	// Need to register model in init
	orm.RegisterModel(new(User), new(Profile))
}
