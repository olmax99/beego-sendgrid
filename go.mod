module gitlab.com/olmax99/beego-sendgrid

go 1.16

require (
	github.com/beego/beego/v2 v2.0.1
	github.com/goombaio/namegenerator v0.0.0-20181006234301-989e774b106e
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/sendgrid/rest v2.6.4+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.10.0+incompatible
)
